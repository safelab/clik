import sys, os
import binascii
import socket
import struct
import atexit
import re
import argparse

# M221 message (modbus payload after the function code) offset in TCP payload
M221_OFFSET = 8 
MODBUS_PORT = 502

CONF_FILE_NAME = "d4fe0100"

# M221 reply sequence number offset in TCP payload
#M221_SEQ_OFFSET = 20


# Include ARP Poisoning !!!!!!!!! => in another python file


class M221_file():
    def __init__(self, address, data_type, data_size, data):
        self.address = address
        self.data_type = data_type
        self.data_size = data_size
        self.data = data 

class Virtual_m221():
    def __init__(self, template_file, fileDir):
        self.m221_template = []
        self.m221_files = []
        self.m221_sid = 0
        # checksum of currently loaded files: it is stored at the end of the configuration file d4fe0100
        self.fileDir = fileDir
        self.load_template(template_file)
        self.load_files()

        # Used in resonse message for the request: 5a:00:24:0a:02:00:32:00:01:00:02 ....
        # self.m221_seq = 0

        # for evaluation only
        self.use_template_num = 0
        self.generate_read_num = 0
        self.generate_write_num = 0

    def __enter__(self):
#        print "__enter__"
        return self

    def __exit__(self, exc_type, exc_value, traceback):
#        print "__exit__"
        print "Use template: ", self.use_template_num
        print "generate read response: ", self.generate_read_num
        print "generate write response: ", self.generate_write_num
        try:
            self.conn.close()
            self.sock.shutdown(socket.SHUT_RDWR)
            self.sock.close()
        except AttributeError as e:
            print "AttributeError: {0}".format(e)
        except:
            print "Other exceptions"
    
    def update_template_chksum(self, chksum):
        chksum_req_payload = '\x00\x04'
        chksum_req_payload2 = self.m221_sid + '\x81\x00\x00\x00\x00'
    
        for i in range(len(self.m221_template)):
            # correct checksum response for the requset payload '00:04'
            if self.m221_template[i][0] == chksum_req_payload:
                res = bytearray(self.m221_template[i][1])
                res[6], res[10] = chksum[2], chksum[2]
                res[7], res[11] = chksum[3], chksum[3]
                res[8], res[12] = chksum[0], chksum[0]
                res[9], res[13] = chksum[1], chksum[1]
                res = bytes(res)
                self.m221_template[i] = (self.m221_template[i][0], res)
            # correct checksum response for the request payload 'sid:81:00:00:00:00'
            elif self.m221_template[i][0] == chksum_req_payload2:
                res = bytearray(self.m221_template[i][1])
                res[3] = chksum[3]
                res[4] = chksum[2]
                res[5] = chksum[1]
                res[6] = chksum[0]
                res = bytes(res)
                self.m221_template[i] = (self.m221_template[i][0], res)

    def load_template(self, template_file):
        sid_req_payload = '\x00\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'

        lines = list(open(template_file, "r"))

        i = 0
        while i < len(lines):
            # comment lines
            if lines[i][0] == "#":
                i += 1
                continue            

            # check validity of request and response pair
            if lines[i][:3] != "-->" or lines[i+1][:3] != "<--":
                print "Error in the template file format: " + template_file
                sys.exit(1)

            req_line = re.sub('>|<|-|:', '', lines[i])
            req = re.sub(r'\s+', '', req_line).decode("hex")

            
            res_line = re.sub('>|<|-|:', '', lines[i+1])
            res = re.sub(r'\s+', '', res_line).decode("hex")
        
            # set m221 session id
            if req == sid_req_payload:
                self.m221_sid = res[-1]
                print "m221 session id: " + binascii.hexlify(self.m221_sid)

            self.m221_template.append((req,res))
            i += 2

    def load_files(self):
        for filename in os.listdir(str(os.path.abspath(self.fileDir))):
            # wrong file
            if len(filename) != 8:
                continue
            filePath = str(os.path.abspath(self.fileDir)) + "/" + filename
            print "Load file: " + filePath

            data_file = open(filePath, "r")
            data = data_file.read()

            remained_data = len(data)
            cur = 0

            address = struct.unpack("<H", filename[:4].decode("hex"))[0]
            data_type = filename[4:8].decode("hex")

            # Set checksum value
            if filename == CONF_FILE_NAME:
                chksum = data[-4:]
                self.update_template_chksum(chksum)

            while (remained_data > 0):
                # maximum payload size in M221 protocol: 0xec
                if remained_data >= 0xec:
                    self.m221_files.append(M221_file(address, data_type, 0xec, data[cur:cur+0xec]))
                    address += 0xec
                    cur += 0xec
                    remained_data -= 0xec
                else:
                    self.m221_files.append(M221_file(address, data_type, remained_data, data[cur:]))
                    remained_data = 0
                           
        #for f in self.m221_files:
        #    print "address: ", hex(f.address)
        #    print "data_type: ", binascii.hexlify(f.data_type)
        #    print "data size: ", hex(f.data_size)
        #    print "data: ", binascii.hexlify(f.data)

            
    def run(self):
        self.open_socket()
    
    def open_socket(self):
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # To bypass TIME_WAIT state (avoid getting the error "[Errno 98] Address already in use")
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.sock.bind(('', MODBUS_PORT))
            self.sock.listen(1)
            self.conn, self.addr = self.sock.accept()
            print "Connected by", self.addr
        except Exception as e:
            print "Failed to open socket: {0}".format(e)

        self.recv_msg()

    def recv_msg(self):
        while 1:
            r_msg = self.conn.recv(1024)
            if len(r_msg) <= M221_OFFSET:
                #print "short message recieved: ", len(r_msg)
                continue
            t = iter(binascii.hexlify(r_msg))
            print "<--" + ":".join(a+b for a,b in zip(t,t))

            self.send_msg(r_msg)
            
            # hanlde disconnect request
            #if r_msg

    def send_msg(self, r_msg):
        modbus_data = self.set_payload(r_msg[M221_OFFSET:])
        # transaction ID(2) + protocol ID(2) + length(2) + unit ID(1) + function code(1)
        modbus_hdr = r_msg[:4] + struct.pack(">H", len(modbus_data) + 2) + r_msg[6:8]

        s_msg = modbus_hdr+modbus_data

        self.conn.send(s_msg)

        t = iter(binascii.hexlify(s_msg))
        print "-->" + ":".join(a+b for a,b in zip(t,t))

    def set_payload(self, req):
        matches = [b for a,b in self.m221_template if req == a]
        if len(matches) == 0:
            # read request
            if req[1] == '\x28':
                self.generate_read_num += 1
                req_data_size = struct.unpack("<H", req[6:8])[0]

                # Find corresponding file
                try:
                    m221_file = next(m221_file for m221_file in self.m221_files if m221_file.address == struct.unpack("<H", req[2:4])[0] and m221_file.data_type == req[4:6] and m221_file.data_size == req_data_size)
                    
                except StopIteration:
                    try:
                        # To handle if 0xd000 data block file contains 20 bytes hash at the end of it. 
                        m221_file = next(m221_file for m221_file in self.m221_files if m221_file.address == struct.unpack("<H", req[2:4])[0] and m221_file.data_type == req[4:6])
                        # if address, and type match, but size is not match:
                        self.m221_files.append(M221_file(m221_file.address + req_data_size, m221_file.data_type, m221_file.data_size - req_data_size, m221_file.data[req_data_size:]))
                    except StopIteration:    
                        print "There is no matching file" 
                        sys.exit(1)
#                return '\x00\xfe' + struct.pack("<H", m221_file.data_size) + m221_file.data
                return '\x00\xfe' + req[6:8] + m221_file.data[:req_data_size]
            # write request
            elif req[1] == '\x29':
                self.generate_write_num += 1

                chksum = m221_write_file(req, self.fileDir)
                if chksum != None:
                    self.update_template_chksum(chksum)
                # reply with success (0xfe)
                return self.m221_sid + '\xfe'
            else:
                print "There is no matching request in the template"
                sys.exit(1)
        else:
            self.use_template_num += 1
            return matches[0]

# write files. if a file has checksum value, return the checksum
def m221_write_file(req, fileDir):
    if "next_addr" not in m221_write_file.__dict__: m221_write_file.next_addr = 0

    addr = struct.unpack("<H", req[2:4])[0]
    data_size = struct.unpack("<H", req[6:8])[0]

    # continuous
    if addr == m221_write_file.next_addr:    
        m221_write_file.f.write(req[8:])
        # the last segment of the configuration file (d4fe0100) which contains checksum value of entire (?) files
        if addr == 0xffc0:
            m221_write_file.f.close()
            conf_file_path = str(os.path.abspath(fileDir)) + "/" + CONF_FILE_NAME
            conf_file = open(conf_file_path, "r")
            conf_data = conf_file.read()
            conf_file.close()
            
            m221_write_file.next_addr = addr + data_size
            # return checksum
            return conf_data[-4:]
    # make a new file
    else:
        try:
            # close previous file if exists
            m221_write_file.f.close()
        except:
            pass

        fileName = binascii.hexlify(req[2:6])
        filePath = str(os.path.abspath(fileDir)) + "/" + fileName
        m221_write_file.f = open(filePath, "w")
        print "Create file: " + filePath
        m221_write_file.f.write(req[8:])

    m221_write_file.next_addr = addr + data_size
    return None

def main():
    parser = argparse.ArgumentParser(description="Fake M221 PLC. Receiving upload requests, upload the files in fileDir. Receiving download requests, update the files in fildDir")
    parser.add_argument("template", help="template file of request-response pairs of M221 protocol message")
    parser.add_argument("fileDir", help="directory where fake M221 files are maintained")
    args = parser.parse_args()

    with Virtual_m221(args.template, args.fileDir) as m221:
        m221.run()

if __name__ == '__main__':
    main()
